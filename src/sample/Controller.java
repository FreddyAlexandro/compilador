package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.control.*;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML ListView<String> lsITokens;
    @FXML ListView<String> lsSugerencias;
    @FXML Label lbresultado;
    @FXML Label lbAccion;

    @FXML TextField tfInput;
    @FXML TextField tfNuevo;
    @FXML Button btnAgregar;

    Alert Result = new Alert(Alert.AlertType.ERROR);

    Conexion con;

    ObservableList<String> items = FXCollections.observableArrayList ();
    ObservableList<String> sugerenciaItems = FXCollections.observableArrayList ();
    @Override
    public void initialize(URL arg, ResourceBundle rb) {
        lsITokens.setOrientation(Orientation.HORIZONTAL);
        lbresultado.setVisible(false);

        tfNuevo.setVisible(false);
        lbAccion.setVisible(false);
        lbAccion.setText("Agregar verbo ?");
        btnAgregar.setVisible(false);

        lsITokens.setItems(items);

    }

    @FXML private void Analizar(){

        try {
            con = new Conexion("root","","Compiladores");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String entrada = tfInput.getText();
        String[] split = entrada.split(" ");
        ResultSet rs;


        int Estado = 0, index = 0;

        while (index <= split.length){

            switch (Estado){

                case 0:

                    if (con.buscar("SELECT sujeto FROM  pronombre WHERE sujeto = '"+split[index]+"';") != null){
                        Estado = 1;
                        items.add("Sujeto");
                        break;

                    }


                    if (con.buscar("SELECT ID_Auxiliar FROM Auxiliar WHERE ID_Auxiliar = '"+split[index]+"';") != null){
                        Estado = 10;
                        items.add("Auxiliar");
                        break;
                    }

                    if (con.buscar("SELECT ID_Wh_Question FROM Wh_Question WHERE ID_Wh_Question = '"+split[index]+"';") != null){
                        Estado = 19;
                        items.add("Wh_Question");
                        break;
                    }

                    Estado = 29;


                    break;

                case 1:
                    if (index == split.length){
                        Estado = 101;
                        break;
                    }

                    if (split[0].compareTo("he") == 0 || split[0].compareTo("she") == 0 || split[0].compareTo("it") == 0){

                        if (con.buscar("SELECT ver_3per FROM Verbos WHERE ver_3per ='"+split[index]+"';") != null){

                            Estado = 2;
                            items.add("Verbo");
                            break;
                        }
                        else {
                            Estado = 201;
                            break;
                        }
                    }


                    if (con.buscar("SELECT ID_Verbo FROM Verbos WHERE ID_Verbo ='"+split[index]+"';") != null){


                        Estado = 2;
                        items.add("Verbo");
                        break;
                    }

                    if (con.buscar("SELECT ID_Auxiliar FROM Auxiliar WHERE ID_Auxiliar = '"+split[index]+"';") != null){
                        Estado = 6;
                        items.add("Auxiliar");
                        break;
                    }

                    Estado = 101;

                    break;

                case 2:
                    if (index == split.length){
                        Estado = 3;
                        break;
                    }

                    if (con.buscar("SELECT ID_Verbo FROM Verbos WHERE ID_Verbo ='"+split[index]+"';") == null
                        && con.buscar("SELECT ID_Auxiliar FROM Auxiliar WHERE ID_Auxiliar = '"+split[index]+"';") == null
                            && con.buscar("SELECT sujeto FROM  pronombre WHERE sujeto = '"+split[index]+"';") == null
                            && con.buscar("SELECT ID_Wh_Question FROM Wh_Question WHERE ID_Wh_Question = '"+split[index]+"';") == null
                            ){
                        items.add("Complemento");
                        Estado = 4;
                        break;
                    }else {
                        items.add("error");
                        Estado = 401;
                    }

                    break;

                case 4:
                    if (index == split.length){
                        Estado = 5;
                        break;
                    }
                    break;

                case 6:
                    if (index == split.length){
                        items.add("error");
                        Estado = 601;
                        break;
                    }

                    rs = con.buscar("SELECT ID_Auxiliar FROM Auxiliar WHERE ID_Auxiliar = '"+split[index]+"';");

                    if (rs != null){
                        items.add("Auxiliar");

                        if (split[0].compareTo("he") == 0 || split[0].compareTo("she") == 0 || split[0].compareTo("it") == 0){

                            if (split[index-1].compareTo("does") != 0){
                                Estado = 702;
                                break;
                            }
                            Estado = 7;
                            break;

                        }else {
                            if (split[index-1].compareTo("do") == 0){
                                Estado = 7;
                                break;
                            }else {
                                Estado = 702;
                                break;
                            }

                        }

                    }

                    items.add("error");
                    Estado = 701;

                    break;

                case 7:
                    if (index == split.length){
                        items.add("error");
                        Estado = 701;
                        break;
                    }

                    if (con.buscar("SELECT ID_Verbo FROM Verbos WHERE ID_Verbo ='"+split[index]+"';") != null){
                        items.add("verbo");
                        Estado = 8;
                    } else {
                        items.add("error");
                        Estado = 701;
                    }
                    break;

                case 8:
                    if (index == split.length){
                        Estado = 9;
                        break;
                    }else {
                        items.add("error");
                        Estado = 801;
                    }
                    break;

                case 10:

                    if (index == split.length){
                        items.add("error");
                        Estado = 1001;
                        break;
                    }

                    if (con.buscar("SELECT sujeto FROM  pronombre WHERE sujeto = '"+split[index]+"';") != null){
                        items.add("Sujeto");

                        if (split[0].compareTo("does") == 0){

                            if (split[index].compareTo("he") == 0 || split[index].compareTo("she") == 0 || split[index].compareTo("it") == 0){
                                Estado = 11;
                                break;
                            }

                            else {
                                Estado = 1002;
                                break;
                            }


                        }else {
                            if (split[index-1].compareTo("do") == 0){
                                if (split[index].compareTo("i") == 0 || split[index].compareTo("you") == 0 || split[index].compareTo("we") == 0 || split[index].compareTo("they") == 0){
                                    Estado = 11;
                                    break;
                                }
                                Estado = 1002;
                                break;

                            }

                        }

                        Estado = 11;
                        break;
                    }

                    items.add("Error");
                    Estado = 1001;
                    break;


                case 11:

                    if (index == split.length){
                        items.add("error");
                        Estado = 1101;
                        break;
                    }

                    if (con.buscar("SELECT ID_Verbo FROM Verbos WHERE ID_Verbo ='"+split[index]+"';") != null){
                        items.add("verbo");
                        Estado = 16;
                        break;
                    }

                    if (con.buscar("SELECT ID_Auxiliar FROM Auxiliar WHERE ID_Auxiliar = '"+split[index]+"';") != null){
                        items.add("Auxiliar");
                        Estado = 12;
                        break;
                    }

                    break;

                case 12:
                    if (index == split.length){
                        items.add("error");
                        Estado = 1201;
                        break;
                    }

                    if (con.buscar("SELECT ID_Verbo FROM Verbos WHERE ID_Verbo ='"+split[index]+"';") != null){
                        items.add("verbo");
                        Estado = 13;
                        break;
                    }

                    items.add("error");
                    Estado = 1201;
                    break;

                case 13:
                    if (index == split.length){
                        items.add("error");
                        Estado = 1301;
                        break;
                    }

                    if (con.buscar("SELECT ID_Verbo FROM Verbos WHERE ID_Verbo ='"+split[index]+"';") == null
                            && con.buscar("SELECT ID_Auxiliar FROM Auxiliar WHERE ID_Auxiliar = '"+split[index]+"';") == null
                            && con.buscar("SELECT sujeto FROM  pronombre WHERE sujeto = '"+split[index]+"';") == null
                            && con.buscar("SELECT ID_Wh_Question FROM Wh_Question WHERE ID_Wh_Question = '"+split[index]+"';") == null
                            ){
                        items.add("Complemento");
                        Estado = 14;
                        break;
                    }else {
                        items.add("error");
                        Estado = 1301;
                    }

                    break;

                case 14:

                    if (split[index-1].indexOf("?") > 0){
                        items.add("Interrogacion");
                        Estado = 15;
                        break;
                    }


                    if (index == split.length){
                        items.add("error");
                        Estado = 1401;
                        break;
                    }


                    if ( split[index].compareTo("?") == 0){
                        items.add("Interrogacion");
                        Estado = 15;
                        break;
                    }else {
                        items.add("error");
                        Estado = 1401;
                        break;
                    }


                case 16:
                    if (index == split.length){
                        items.add("error");
                        Estado = 1601;
                        break;
                    }

                    if (con.buscar("SELECT ID_Verbo FROM Verbos WHERE ID_Verbo ='"+split[index]+"';") == null
                            && con.buscar("SELECT ID_Auxiliar FROM Auxiliar WHERE ID_Auxiliar = '"+split[index]+"';") == null
                            && con.buscar("SELECT sujeto FROM  pronombre WHERE sujeto = '"+split[index]+"';") == null
                            && con.buscar("SELECT ID_Wh_Question FROM Wh_Question WHERE ID_Wh_Question = '"+split[index]+"';") == null
                            ){
                        items.add("Complemento");
                        Estado = 17;
                        break;
                    }else {
                        items.add("error");
                        Estado = 1601;
                    }
                    break;

                case 17:

                    if (index == split.length){
                        if (split[index-1].indexOf("?") > 0){
                            items.add("Interrogacion");
                            Estado = 18;
                            break;
                        } else {
                            items.add("error");
                            Estado = 1701;
                            break;
                        }

                    }else {
                        if ( split[index].compareTo("?") == 0){
                            items.add("Interrogacion");
                            Estado = 18;
                            break;
                        }else {
                            items.add("error");
                            Estado = 1701;
                            break;
                        }
                    }


                case 19:
                    if (index == split.length){
                        items.add("error");
                        Estado = 1901;
                        break;
                    }

                    if (con.buscar("SELECT ID_Verbo FROM Verbos WHERE ID_Verbo ='"+split[index]+"';") != null){
                        items.add("verbo");
                        Estado = 20;
                        break;
                    } // Podria agregar un estado para agregar el verbo

                    if (con.buscar("SELECT ID_Auxiliar FROM Auxiliar WHERE ID_Auxiliar = '"+split[index]+"';") != null){
                        items.add("Sujeto");
                        Estado = 24;
                        break;
                    }


                    items.add("error");
                    Estado = 1901;

                    break;


                case 20:

                    if (index == split.length){
                        items.add("error");
                        Estado = 2001;
                        break;
                    }

                    if (con.buscar("SELECT sujeto FROM  pronombre WHERE sujeto = '"+split[index]+"';") != null){
                        items.add("Sujeto");
                        Estado = 21;
                        break;
                    }

                    items.add("error");
                    Estado = 2001;

                    break;

                case 21:
                    if (index == split.length){
                        items.add("error");
                        Estado = 2101;
                        break;
                    }

                    if (con.buscar("SELECT ID_Verbo FROM Verbos WHERE ID_Verbo ='"+split[index]+"';") == null
                            && con.buscar("SELECT ID_Auxiliar FROM Auxiliar WHERE ID_Auxiliar = '"+split[index]+"';") == null
                            && con.buscar("SELECT sujeto FROM  pronombre WHERE sujeto = '"+split[index]+"';") == null
                            && con.buscar("SELECT ID_Wh_Question FROM Wh_Question WHERE ID_Wh_Question = '"+split[index]+"';") == null
                            ){
                        items.add("Complemento");
                        Estado = 22;
                        break;
                    }else {
                        items.add("error");
                        Estado = 2101;
                    }
                    break;


                case 22:
                    if (index == split.length){
                        if (split[index-1].indexOf("?") > 0){
                            items.add("Interrogacion");
                            Estado = 23;
                            break;
                        } else {
                            items.add("error");
                            Estado = 2201;
                            break;
                        }

                    }else {
                        if ( split[index].compareTo("?") == 0){
                            items.add("Interrogacion");
                            Estado = 23;
                            break;
                        }else {
                            items.add("error");
                            Estado = 2201;
                            break;
                        }
                    }



                case 24:
                    if (index == split.length){
                        items.add("error");
                        Estado = 2401;
                        break;
                    }
                    if (con.buscar("SELECT ID_Verbo FROM Verbos WHERE ID_Verbo ='"+split[index]+"';") != null){
                        items.add("verbo");
                        Estado = 25;
                        break;
                    } // Podria agregar un estado para agregar el verbo

                    items.add("error");
                    Estado = 2401;

                    break;



                case 25:
                    if (index == split.length){
                        items.add("error");
                        Estado = 2501;
                        break;
                    }

                    if (con.buscar("SELECT sujeto FROM  pronombre WHERE sujeto = '"+split[index]+"';") != null){
                        items.add("Sujeto");
                        Estado = 26;
                        break;
                    }

                    items.add("error");
                    Estado = 2501;
                    break;



                case 26:

                    if (index == split.length){
                        items.add("error");
                        Estado = 2501;
                        break;
                    }

                    if (con.buscar("SELECT ID_Verbo FROM Verbos WHERE ID_Verbo ='"+split[index]+"';") == null
                            && con.buscar("SELECT ID_Auxiliar FROM Auxiliar WHERE ID_Auxiliar = '"+split[index]+"';") == null
                            && con.buscar("SELECT sujeto FROM  pronombre WHERE sujeto = '"+split[index]+"';") == null
                            && con.buscar("SELECT ID_Wh_Question FROM Wh_Question WHERE ID_Wh_Question = '"+split[index]+"';") == null
                            ){
                        items.add("Complemento");
                        Estado = 27;
                        break;
                    }else {
                        items.add("error");
                        Estado = 2601;
                    }

                    break;



                case 27:

                    if (index == split.length){
                        if (split[index-1].indexOf("?") > 0){
                            items.add("Interrogacion");
                            Estado = 28;
                            break;
                        } else {
                            items.add("error");
                            Estado = 2701;
                            break;
                        }

                    }else {
                        if ( split[index].compareTo("?") == 0){
                            items.add("Interrogacion");
                            Estado = 28;
                            break;
                        }else {
                            items.add("error");
                            Estado = 2701;
                            break;
                        }
                    }

                case 29:
                    Result.setTitle("Error");
                    Result.setHeaderText("Esperaba sujeto, un verbo, un Wh_Question o un auxiliar");
                    Result.showAndWait();
                    break;
                default:
                    break;
            }
            index++;

        }

        switch (Estado){
            case 101:
                lsITokens.setItems(items);
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un verbo o un auxiliar");
                Result.showAndWait();

                rs = con.buscar("SELECT * FROM Verbos;");
                try {

                    rs.first();
                    sugerenciaItems.add("Verbos".toUpperCase());
                    while (rs != null){
                        sugerenciaItems.add(rs.getString("ID_Verbo"));
                        rs.next();
                    }

                } catch (SQLException e) {
                    System.out.println("Error");
                }
                sugerenciaItems.add(" ");
                rs = con.buscar("SELECT * FROM Auxiliar;");

                try {

                    rs.first();
                    sugerenciaItems.add("Auxiliares".toUpperCase());
                    while (rs != null){
                        sugerenciaItems.add(rs.getString("ID_Auxiliar"));
                        rs.next();
                    }
                    sugerenciaItems.add("");

                } catch (SQLException e) {
                    System.out.println("Error");
                }

                lsSugerencias.setItems(sugerenciaItems);


                tfNuevo.setVisible(true);
                lbAccion.setVisible(true);
                btnAgregar.setVisible(true);

                btnAgregar.setOnAction(event -> {
                    add_Verbo(tfNuevo.getText());
                });
                break;

            case 201:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Verbo mal conjugado");
                Result.showAndWait();
                break;

            case 3:
                lsITokens.setItems(items);
                lbresultado.setText("Correcto !");
                lbresultado.setVisible(true);
                break;

            case 401:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un complemento");
                Result.showAndWait();
                break;

            case 5:
                lsITokens.setItems(items);
                lbresultado.setText("Correcto !");
                lbresultado.setVisible(true);
                break;

            case 601:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un auxiliar");
                Result.showAndWait();
                break;

            case 701:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un verbo");
                Result.showAndWait();
                break;

            case 702:
                // error de auxiliar
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Auxiliar incorrecto");
                Result.showAndWait();
                break;

            case 801:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Sintaxis desconocida");
                Result.showAndWait();
                break;

            case 9:
                lsITokens.setItems(items);
                lbresultado.setText("Correcto !");
                lbresultado.setVisible(true);
                break;

            case 1001:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un sujeto");
                Result.showAndWait();
                break;

            case 1002:
                // Error auxiliar
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Auxiliar incorrecto");
                Result.showAndWait();
                break;

            case 1101:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un verbo o auxiliar");
                Result.showAndWait();
                break;

            case 1201:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un verbo");
                Result.showAndWait();
                break;

            case 1301:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un complemento");
                Result.showAndWait();
                break;

            case 1401:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un signo de interrogacion");
                Result.showAndWait();
                break;

            case 15:
                lsITokens.setItems(items);
                lbresultado.setText("Correcto !");
                lbresultado.setVisible(true);
                break;

            case 1601:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un complemento");
                Result.showAndWait();
                break;

            case 1701:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un signo de interrogacion");
                Result.showAndWait();
                break;

            case 18:
                lsITokens.setItems(items);
                lbresultado.setText("Correcto !");
                lbresultado.setVisible(true);
                break;

            case 1901:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un sujeto o un verbo");
                Result.showAndWait();
                break;

            case 2001:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un sujeto");
                Result.showAndWait();
                break;

            case 2101:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un complemento");
                Result.showAndWait();
                break;

            case 2201:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un signo de interrogacion");
                Result.showAndWait();
                break;

            case 23:
                lsITokens.setItems(items);
                lbresultado.setText("Correcto !");
                lbresultado.setVisible(true);
                break;

            case 2401:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un verbo");
                Result.showAndWait();
                break;

            case 2501:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un sujeto");
                Result.showAndWait();
                break;

            case 2601:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un complemento");
                Result.showAndWait();
                break;

            case 2701:
                lbresultado.setText("Error");
                Result.setTitle("Error");
                Result.setHeaderText("Esperaba un signo de interrogacion");
                Result.showAndWait();
                break;

            case 28:
                lsITokens.setItems(items);
                lbresultado.setText("Correcto !");
                lbresultado.setVisible(true);
                break;


            default:
                break;

        }

        con.cerrar();

    }

    private void add_Verbo (String verbo){

        Conexion con2 = null;
        try {
            con2 = new Conexion("root","","Compiladores");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        con2.insertar("INSERT INTO Verbos VALUES ('"+verbo+"');");
        con2.cerrar();
        restart();
        tfNuevo.clear();
        tfNuevo.setVisible(false);
        lbAccion.setVisible(false);
        btnAgregar.setVisible(false);

    }

   
    @FXML private void restart(){
        tfNuevo.setVisible(false);
        lbAccion.setVisible(false);
        btnAgregar.setVisible(false);
        lbresultado.setVisible(false);
        items.clear();
        sugerenciaItems.clear();

    }

}
