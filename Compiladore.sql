-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 23-06-2016 a las 20:42:31
-- Versión del servidor: 10.0.17-MariaDB
-- Versión de PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Compiladores`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Auxiliar`
--

CREATE TABLE `Auxiliar` (
  `ID_Auxiliar` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Auxiliar`
--

INSERT INTO `Auxiliar` (`ID_Auxiliar`) VALUES
('do'),
('does'),
('not'),
('am'),
('is'),
('are');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pronombre`
--

CREATE TABLE `pronombre` (
  `sujeto` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pronombre`
--

INSERT INTO `pronombre` (`sujeto`) VALUES
('i'),
('you'),
('he'),
('she'),
('it'),
('we'),
('they');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `verbos`
--

CREATE TABLE `verbos` (
  `ID_Verbo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `verbos`
--

INSERT INTO `verbos` (`ID_Verbo`) VALUES
('play'),
('run'),
('walk'),
('eat'),
('drive'),
('like'),
('want'),
('paint');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Wh_Question`
--

CREATE TABLE `Wh_Question` (
  `ID_Wh_Question` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Wh_Question`
--

INSERT INTO `Wh_Question` (`ID_Wh_Question`) VALUES
('what'),
('where'),
('when'),
('who'),
('whose'),
('why'),
('which'),
('how');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
